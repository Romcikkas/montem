<?php
add_filter( 'woocommerce_product_tabs', 'product_enquiry_tab' );
function product_enquiry_tab( $tabs ) {

    $tabs['test_tab'] = array(
        'title'     => __( 'Teirautis apie prekę', 'woocommerce' ),
        'priority'  => 50,
        'callback'  => 'product_enquiry_tab_form'
    );

    return $tabs;

}
function product_enquiry_tab_form() {
    global $product;
    //If you want to have product ID also
    //$product_id = $product->id;
    $subject    =   "Informacija apie: ".$product->post->post_title;

    echo do_shortcode('[contact-form-7 id="874" title="Contact Form Flat_product name"]'); //add your contact form shortcode here ..

    ?>

    <script>
    (function($){
        $(".product_name").val("<?php echo $subject; ?>");
    })(jQuery);
    </script>   
    <?php   
}
    ?>