��    &      L  5   |      P     Q     _     k     �     �     �     �  #   �  <   �  ,   1     ^  	   m     w     �  *   �     �     �     �  %   �  '        G  @   Y     �     �  )   �  #   �  *        1  
   B  
   M  A   X     �     �     �  B   �     �        �       �     �     �     	     	     2	     M	  .   f	  ^   �	  7   �	     ,
     C
     K
     b
  2   v
     �
  +   �
     �
  /     *   5     `  B   �     �     �  8   �  4     3   Q     �     �     �  M   �     �     	            	   ;  #   E         	   &       $                                           
         !                            "                                                         #   %                   All countries Description Display payment methods list Enable Paysera Enable Paysera payment Enable grid view Enable test mode Enable this to accept test payments Enable this to display payment methods list at checkout page Enable this to use payment methods grid view Extra Settings Grid view List of payments Main Settings Make payment method choice on Paysera page New Order Status New order creation status Order Status Order status after completing payment Order status with not finished checkout Paid Order Status Payment method title that the customer will see on your website. Paysera Paysera sign password Paysera: Callback order payment completed Paysera: Customer came back to page Paysera: Order checkout process is started Pending checkout Project ID Project id Select which country payment methods to display (empty means all) Sign Specific countries Test This controls the description which the user sees during checkout. Title http://www.paysera.com Project-Id-Version: WooCommerce Payment Gateway - Paysera 2.4.5
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/woo-payment-gateway-paysera
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2018-03-09 11:26+0200
Language-Team: 
X-Generator: Poedit 2.0.6
Last-Translator: 
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : 2);
Language: lv_LV
 Visas valstis Apraksts Rādīt apmaksas veidu sarakstu Iespējot Paysera Iespējot Paysera maksājumus Iespējot CSS izmantošanu Iespējot testa režīmu Iespējojiet šo, lai veiktu testa maksājumus Iespējojiet šo, lai pasūtījuma noformēšanas lapā būtu redzami pieejamie apmaksas veidi Iespējojiet šo, lai mainītu spraudņa izskatu ar CSS Papildus iestatījumi  Izskats Apmaksas veidu sarakts Pamata iestatījumi Apraksts, kuru pircēji redzēs jūsu mājas lapā Jauna pasūtījuma statuss Pasūtījuma statuss pēc tā izveidošanas Pasūtījumu statusi Pasūtījuma statuss pēc maksājuma veikšanas Pasūtījuma statuss ar nepabeigtu apmaksu Apmaksāta pasūtījuma statuss Apmaksas veida nosaukums, kuru pircēji redzēs jūsu mājas lapā Paysera Paysera projekta parole Paysera: Pasūtījuma statuss pēc maksājuma veikšanas Paysera: Pasūtījuma statuss pēc tā izveidošanas Paysera: Pasūtījuma statuss ar nepabeigtu apmaksu Gaida apmaksu Projekta ID Projekta ID Izvēlieties, kuru valstu apmaksas veidi tiks rādīti (tukš nozīmē visas) Projekta parole Valstis Testēšana Maksāt caur Paysera sistēmu Nosaukums https://www.paysera.com/v2/lv/index 