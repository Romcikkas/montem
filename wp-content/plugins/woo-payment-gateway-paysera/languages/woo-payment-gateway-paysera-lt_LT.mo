��    (      \  5   �      p     q          �     �     �     �     �  #   �  <     ,   Q     ~  	   �     �     �  *   �     �     �       %     '   ?     g  @   y     �     �  )   �  #     *   &     Q     m  
   ~  
   �  A   �     �     �     �  B   �     6     <     V  �  m     =	  
   J	  !   U	     w	      �	  !   �	     �	  3   �	  P   
  O   m
     �
     �
     �
                5     M     m           �     �  E   �           (  )   F  )   p  &   �  (   �     �            W   +     �     �     �  2   �     �  #   �  #            	   (       %                                           
         "                             #      '      !                                          $   &                   All countries Description Display payment methods list Enable Paysera Enable Paysera payment Enable grid view Enable test mode Enable this to accept test payments Enable this to display payment methods list at checkout page Enable this to use payment methods grid view Extra Settings Grid view List of payments Main Settings Make payment method choice on Paysera page New Order Status New order creation status Order Status Order status after completing payment Order status with not finished checkout Paid Order Status Payment method title that the customer will see on your website. Paysera Paysera sign password Paysera: Callback order payment completed Paysera: Customer came back to page Paysera: Order checkout process is started Paysera: Status changed to  Pending checkout Project ID Project id Select which country payment methods to display (empty means all) Sign Specific countries Test This controls the description which the user sees during checkout. Title WooCommerce is not active http://www.paysera.com Project-Id-Version: WooCommerce Payment Gateway - Paysera 2.4.5
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/woo-payment-gateway-paysera
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2018-03-09 11:01+0200
Language-Team: 
X-Generator: Poedit 2.0.6
Last-Translator: 
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : 2);
Language: lt_LT
 Visos šalys Aprašymas Rodyti mokėjimo būdų sąrašą Aktyvuoti Paysera Aktivuoti mokėjimus per Paysera Aktyvuoti atvaizdavimą tinkleliu Įjungti testavimo režimą Aktyvuokite, norėdami priimti testinius mokėjimus Aktivuokite, kad apmokėjimo puslapyje būtų rodomas mokėjimo būdų sąrašas Aktyvuokite, norėdami pakeisti apmokėjimo būdų atvaizdavimą į tinklelinį Papildomi nustatymai  Atvaizdavimas tinkleliu Mokėjimų sąrašas Pagrindiniai nustatymai Mokėti per Paysera sistemą Naujo užsakymo būsena Naujo užsakymo kūrimo būsena Užsakymo būsena Užsakymo būsena po apmokėjimo Neapmokėto užsakymo būsena Apmokėto užsakymo būsena Mokėjimo būdo pavadinimas, kurį klientas matys jūsų svetainėje. Paysera Paysera projekto slaptažodis Paysera: Užsakymo būsena po apmokėjimo Paysera: Naujo užsakymo kūrimo būsena  Paysera: Neapmokėto užsakymo būsena Paysera: Apmokėjimo būsena pakeista į Laukiantys apmokėjimo Projekto ID Paysera projekto numeris (ID) Pasirinkite, kurių šalių apmokėjimo būdus rodyti (tuščias laukas reiškia visas) Sign Konkrečios šalys Testuoti Aprašymas, kurį atsiskaitymo metu mato klientas. Pavadinimas WooCommerce įskiepis nėra aktyvus https://www.paysera.com/v2/lt/index 