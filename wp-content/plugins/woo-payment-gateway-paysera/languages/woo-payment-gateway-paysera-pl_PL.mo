��    &      L  5   |      P     Q     _     k     �     �     �     �  #   �  <   �  ,   1     ^  	   m     w     �  *   �     �     �     �  %   �  '        G  @   Y     �     �  )   �  #   �  *        1  
   B  
   M  A   X     �     �     �  B   �     �        �       �     �  &   �     	     .	     J	     V	  *   k	  S   �	  @   �	     +
     @
     E
     W
     k
     �
  #   �
     �
  .   �
  2   
     =  D   \     �     �  7   �  -   �  ;   '     c     u     �  D   �     �     �     �  ;   �     )  #   0         	   &       $                                           
         !                            "                                                         #   %                   All countries Description Display payment methods list Enable Paysera Enable Paysera payment Enable grid view Enable test mode Enable this to accept test payments Enable this to display payment methods list at checkout page Enable this to use payment methods grid view Extra Settings Grid view List of payments Main Settings Make payment method choice on Paysera page New Order Status New order creation status Order Status Order status after completing payment Order status with not finished checkout Paid Order Status Payment method title that the customer will see on your website. Paysera Paysera sign password Paysera: Callback order payment completed Paysera: Customer came back to page Paysera: Order checkout process is started Pending checkout Project ID Project id Select which country payment methods to display (empty means all) Sign Specific countries Test This controls the description which the user sees during checkout. Title http://www.paysera.com Project-Id-Version: WooCommerce Payment Gateway - Paysera 2.4.5
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/woo-payment-gateway-paysera
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2018-07-30 09:44+0300
Language-Team: 
X-Generator: Poedit 2.0.6
Last-Translator: 
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
Language: pl_PL
 Wszystkie kraje Opis Wyświetl listę sposobów płatności Aktywuj Paysera Aktywuj płatności Paysera Aktywuj CSS Aktywuj tryb testowy Zaznacz aby włączyć płatności testowe Włącz tę opcję, aby wyświetlić listę metod płatności na stronie transakcji Włącz tę opcję, aby użyć wtyczek z przestylizowaniem w CSS Ustawienia dodatkowe Styl Lista płatności Ustawienia główne Płatność w systemie Paysera Status nowego zamówienia Status tworzenia nowego zamówienia Status zamówienia Status zamówienia po zakończeniu płatności Status zamówienia z niezakończoną płatnością Status opłaconego zamówienia Tytuł sposobu płatności, który klient zobaczy na Twojej stronie. Paysera Hasło projektu Paysera Paysera: Status zamówienia po zakończeniu płatności Paysera: Status tworzenia nowego zamówienia  Paysera: Status zamówienia z niezakończoną płatnością Transakcja w toku ID projektu ID projektu Wybrać kraj, aby wyświetlić płatności (puste oznacza wszystkie) Zaloguj Wybrane kraje Test Opis sposobu płatności, który jest widoczny dla klienta. Tytuł https://www.paysera.com/v2/pl/index 