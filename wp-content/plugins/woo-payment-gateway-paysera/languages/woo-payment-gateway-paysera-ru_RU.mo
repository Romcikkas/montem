��    &      L  5   |      P     Q     _     k     �     �     �     �  #   �  <   �  ,   1     ^  	   m     w     �  *   �     �     �     �  %   �  '        G  @   Y     �     �  )   �  #   �  *        1  
   B  
   M  A   X     �     �     �  B   �     �        �       �     	  ?   	     W	  0   p	  7   �	  ,   �	  S   
  y   Z
  m   �
  0   B  
   s     ~  #   �  2   �  &   �  7        R  F   l  4   �  0   �  }        �  #   �  O   �  A     =   U      �     �     �  �   �     �  %   �     �  d   �     ;  #   L         	   &       $                                           
         !                            "                                                         #   %                   All countries Description Display payment methods list Enable Paysera Enable Paysera payment Enable grid view Enable test mode Enable this to accept test payments Enable this to display payment methods list at checkout page Enable this to use payment methods grid view Extra Settings Grid view List of payments Main Settings Make payment method choice on Paysera page New Order Status New order creation status Order Status Order status after completing payment Order status with not finished checkout Paid Order Status Payment method title that the customer will see on your website. Paysera Paysera sign password Paysera: Callback order payment completed Paysera: Customer came back to page Paysera: Order checkout process is started Pending checkout Project ID Project id Select which country payment methods to display (empty means all) Sign Specific countries Test This controls the description which the user sees during checkout. Title http://www.paysera.com Project-Id-Version: WooCommerce Payment Gateway - Paysera 2.4.5
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/woo-payment-gateway-paysera
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2018-03-09 11:11+0200
Language-Team: 
X-Generator: Poedit 2.0.6
Last-Translator: 
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
Language: ru_RU
 Все страны Описание Отобразить список способов оплаты Включить Paysera Включить оплату через Paysera Активировать использование CSS Включить тестовый режим Активируйте, чтобы получить тестовые платежи Активируйте, чтобы отобразить список способов оплаты на странице. Активируйте, чтобы изменить оформление плагина с помощью CSS Дополнительные настройки  Стиль Список платежей Основные настройки Оплатить через систему Paysera Статус нового заказа Статус создания нового заказа Статус заказа Статус заказа после завершения оплаты Статус неоплаченного заказа Статус оплаченного заказа Название способа оплаты, которое клиент будет видеть на вашем сайте. Paysera Пароль проекта Paysera Paysera: Статус заказа после завершения оплаты Paysera: Статус создания нового заказа  Paysera: Статус неоплаченного заказа В ожидании оплаты ID проекта Номер проекта (ID) Выберите страны, способы оплаты которых будут отображаться (если ничего не выбрано, значит все) Sign Определенные страны Тестировать Это описание, которое пользователь видит входе оплаты. Название https://www.paysera.com/v2/ru/index 