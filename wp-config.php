<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'montem_shopwp' );

/** MySQL database username */
define( 'DB_USER', 'montem_shopwp' );

/** MySQL database password */
define( 'DB_PASSWORD', 'h4OIe36Hwj' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'I.(b!|xgwP4Fj-RbukyttPi&~zI:=UuUv;R2<$u(p^^@ =6Ac!<*-@-pZYRD.:r4' );
define( 'SECURE_AUTH_KEY',  'dv}waOyu37Vecu%}JtXYs4ZFa0l!Fh fB=.QF14E#&$-;hi7:6IY0d?93?XjTA?|' );
define( 'LOGGED_IN_KEY',    'o+@Iu]#8A-iMOg&MBy}Q^qK:/RFNW{*`tKAk%5d&SVh3FQ: {tm=w)eoXd4]s6v;' );
define( 'NONCE_KEY',        'c`Y8dT%R1im/jx<G(J=t9$c{kohZkSgvXz?5xh)37Lf%c>qG-^{,/maBrqy%e]d5' );
define( 'AUTH_SALT',        'vbAiV/7A5{CgLdU8P {vKs[vh6M2`9H=A^(wd<Eijq*@i9H?k]l*D(uoCxk%Z5Rw' );
define( 'SECURE_AUTH_SALT', '=49/o&~79zXj$zzISI!*`C.F;*F] D[>%w6Ta{h(-7~:43vj#u/@UZmIs?ozt7Xa' );
define( 'LOGGED_IN_SALT',   ':%#t!$fCoh$y5f2H>Mu#M4q2T;5i.$S)L%s1k`oiI<+`G!5B5@4T^tC8VbOYP]C,' );
define( 'NONCE_SALT',       '+ducomE]=rKLRXa^)d?P0kxhU556y$+jm*5RMkzf fB+4|R- OB^u wQytd/fn#J' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
